const Message = require("../model/message")

const insertMessage = async data => {
    const newMessage  = new Message(data);
    return await newMessage.save();
};

const getAllMessageByRoom = async (id, role) => {
    console.log(id)
    return await Message.find({ roomId: id });
};

module.exports = {
    insertMessage,
    getAllMessageByRoom
  };