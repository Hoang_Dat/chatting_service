const Rom = require("../model/room")

const insertRoom = async data => {
    const dataExist = await Rom.find({ clientUser : data.clientUser,collaboratorUser:  data.collaboratorUser })
    console.log("dsdfsf", dataExist)
    if(dataExist.length > 0) throw new Error("Data Existed")
    const newRoom  = new Rom(data);
    return await newRoom.save();
};

const getAllRoomByUser = async (userId, role) => {
    console.log(userId)
    console.log(role)
    if(role === "client") 
        return await Rom.find({ clientUser : userId})
    else
        return await Rom.find({ collaboratorUser: userId })
};

module.exports = {
    insertRoom,
    getAllRoomByUser
  };