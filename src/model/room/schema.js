
const mongoose = require('mongoose');

const RoomSchema = new mongoose.Schema({
  clientUser: { type: String, required: true },
  collaboratorUser: { type: String, required: true },
  createdDate: { type: Date, default: Date.now }
});

module.exports = RoomSchema;