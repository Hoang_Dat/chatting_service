const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
  roomId : {type: String, required: true},
  message: { type: String },
  createdBy: { type: String, required: true },
  role : {sype : String },
  createdDate: { type: Date, default: Date.now }
});

module.exports = MessageSchema;