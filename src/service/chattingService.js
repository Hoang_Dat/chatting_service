const { insertMessage,
    getAllMessageByRoom } = require('../handler/messageHandler');
const { insertRoom,
    getAllRoomByUser } = require('../handler/roomHandler');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

const addRoomChat = async (call, callback) => {
    try {
        const data = {
            clientUser: call.request.clientUser,
            collaboratorUser: call.request.collaboratorUser,
        }
        const result = await insertRoom(data);
        callback(null, result)
    } catch (error) {
        console.log(error)
        callback({
            code: grpc.status.INTERNAL,
            details: error.message
        })
    }

}
const getListRoomChatbyUser = async (call, callback) => {
    const roomChats = await getAllRoomByUser(call.request.userId, call.request.role)
    console.log("result", roomChats)
    if (roomChats) {
        callback(null, {roomChattings : roomChats})
    } else {
        callback({
            code: grpc.status.INTERNAL,
            details: "getListRoomChatbyUser faillure"
        })
    }
}

const getListMessageByRoomId = async (call, callback) => {
    console.log("result", call.request.id)
    const messages = await getAllMessageByRoom(call.request.id)
    console.log("result", messages)
    if (messages) {
        callback(null, {chatMessages : messages})
    } else {
        callback({
            code: grpc.status.INTERNAL,
            details: "getListMessageByRoomId faillure"
        })
    }
} 
const insertMessageChat = async(call, callback) => {
    try {
        const data = {
            roomId: call.request.roomId,
            createdBy: call.request.createdBy,
            message: call.request.message,
        }
        console.log("Data request", call.request)
        const result = await insertMessage(data);
        callback(null, result)
    } catch (error) {
        console.log(error)
        callback({
            code: grpc.status.INTERNAL,
            details: "addAppointment faillure"
        })
    }
}
module.exports = {
    addRoomChat,
    getListRoomChatbyUser,
    getListMessageByRoomId,
    insertMessageChat
};
