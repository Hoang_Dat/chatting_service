const chattingDB = require('./src/config/database');
const grpc = require('grpc');
const { 
    addRoomChat,
    getListRoomChatbyUser,
    getListMessageByRoomId,
    insertMessageChat
  } = require('./src/service/chattingService');
const PROTO_PATH = './src/proto/chatting.proto';
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const chatting_proto = grpc.loadPackageDefinition(packageDefinition);
const server = new grpc.Server();
server.addService(chatting_proto.ChattingService.service, {
    InsertRoomChat: addRoomChat,
    ListRoomChatByUser: getListRoomChatbyUser,
    InsertMessageChat: insertMessageChat,
    GetRoomChatDetail: getListMessageByRoomId,
})

server.bind('127.0.0.1:50059',
    grpc.ServerCredentials.createInsecure());
console.log('Server running at http://127.0.0.1:50059')
server.start()